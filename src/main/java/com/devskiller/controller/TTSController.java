package com.devskiller.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("tts/")
public class TTSController {

	@GetMapping(value = "/test", produces = "application/json;charset=UTF-8")
	public String getTest() {
		return "success";
	}
}
