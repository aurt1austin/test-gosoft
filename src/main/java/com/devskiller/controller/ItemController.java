package com.devskiller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devskiller.service.ItemService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ItemController {

	@Autowired
	private ItemService itemService;

	/**
	 * Get items's title with average rating lower than input rating
	 * 
	 * @param bodyRequest a collection Of Request Data
	 * @return a List of item's title
	 */
	@GetMapping(value = "/titles", produces = "application/json;charset=UTF-8")
	public List<String> getTitles(@RequestParam Double rating) {
		List<String> resultList = new ArrayList<>();
		try {
			resultList = itemService.getTitlesWithAverageRatingLowerThan(rating);
		} catch (Exception e) {
			System.out.println("ItemController.getTitles error : " + e);
		}
		return resultList;
	}

}
